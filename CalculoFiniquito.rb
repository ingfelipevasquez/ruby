class Indemnizacion
    def initialize(meses, sueldo)
      @meses = meses
      @sueldo = sueldo
      @monto = 0
    end
    def calcular
      annos_serv = 0
      uf = 28644
      ##calcular si sueldo excede maximo legal de 90UF
      sueldo_max_legal = uf*90
      @sueldo = sueldo_max_legal if @sueldo > sueldo_max_legal
  
      if @meses > 11
        annos_serv = (@meses/12).to_f.round
        annos_serv += 1 if @meses%12 > 5
      end
      @monto = @sueldo*annos_serv
    end
    def resultado
      puts "LA INDEMNIZACION ES : #{@monto}"
    end
  end
  print "INGRESAR SUELDO:"
  sueldo = gets.chomp.to_i
  print "INGRESAR MESES DE ANTIGUEDAD:"
  meses = gets.chomp.to_i
  indemniza = Indemnizacion.new(sueldo,meses)
  indemniza.calcular
  indemniza.resultado