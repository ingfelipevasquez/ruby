class Car
    attr_accessor :make, :model, :year #define geter y seter para acceder a los valores desde la instancia
    @@type = "Car" #define variable de clase
    def initialize(make,model,year)
      @make, @model, @year = make, model, year
    end
    def info
      #forma 1 llamar a bloque
      yield(@make,@model,@year,@@type)
    end
end

class Jeep < Car #herencia
  def info &bloque
    @@type = "Jeep" #asignar valor a variable de clase heredada de Car
    #forma 2 de llamar a bloque
    bloque.call(@make,@model,@year,@@type)
  end
end
class Camioneta < Car
  def info &bloque
    @@type = "Camioneta"
    bloque.call(@make,@model,@year,@@type)
  end
end
class Suv < Car
  def info &bloque
    @@type = "Suv"
    bloque.call(@make,@model,@year,@@type)
  end
end

list = [] # declarar Array
list << Car.new("Honda", "Accord", 2016) #ingresar objeto a Array
list << Camioneta.new("Chevrolet", "Luv", 2015)
list << Suv.new("Kia", "Sportage", 2014)
list << Jeep.new("Mitsubishi","Montero",2005)

list.map do |item| #recorrer Array
    #llamar al metodo info y retornar valores al bloque
    item.info {|m,p,y,t| puts "La marca es: #{m}, el modelo es : #{p}, el año es:#{y}, Tipo:#{t}"}
endT