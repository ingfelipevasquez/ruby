a = [223, 2, 34, 4, 9, 98, 905, 503]
class Array
    def swap(a,b)
        self[a], self[b] = self[b], self[a]
    end
end

begin
  intercambio = false
  (a.count-1).times do |i|
    if a[i].to_s < a[i+1].to_s# Compara los valores como string
      if a[i].digits.reverse[0] < a[i+1].digits.reverse[0] # se compara el primer digito
        a.swap(i,i+1) #se hace intercambio en array
        intercambio = true
      end
    end # if
    next
  end # times
end while intercambio == true # Este ciclo se ejecuta mientras haga intercambios...
 
puts "a = " << a.join("")