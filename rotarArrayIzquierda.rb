arreglo = []

def rotar(cant,arreglo)
  cant.times do
    b = arreglo.shift()
    arreglo.push(b)
  end
  arreglo
end
print "INGRESAR ARREGLO:"

seguir = "N"
contador = 1
begin
  print "\nINGRESAR VALOR N° #{contador}: "
  valor = gets.chomp.to_i
  arreglo.push(valor)
  print arreglo
  contador += 1
  print "\nDESEAS INGRESAR UN NUEVO VALOR S/N: "
  seguir = gets.chomp.upcase
end while (seguir == "S")

print "INGRESAR CANTIDAD DE POSICIONES A ROTAR:"
cantidad = gets.chomp.to_i
print rotar(cantidad,arreglo)